import os
import re
from contextlib import closing
from datetime import datetime

import requests
from requests.exceptions import RequestException
from bs4 import BeautifulSoup

BASE_URI = 'https://www.smashingmagazine.com'
BASE_URL = 'https://www.smashingmagazine.com/category/wallpapers'
BASE_DIR = os.path.dirname(os.path.abspath(__file__))


def is_downloadable(_url):
    """
    Does the url contain a downloadable resource
    """
    h = requests.head(_url, allow_redirects=True)
    header = h.headers
    content_type = header.get('content-type')
    if 'text' in content_type.lower():
        return False
    if 'html' in content_type.lower():
        return False
    return True


def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(requests.get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        print('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200
            and content_type is not None
            and content_type.find('html') > -1)


def get_filename_from_cd(cd):
    """
    Get filename from content-disposition
    """
    if not cd:
        return None
    fname = re.findall('filename=(.+)', cd)
    if len(fname) == 0:
        return None
    return fname[0]


def make_dirs(path):
    if not os.path.exists(path):
        os.makedirs(path)


def find_nodes(raw_html, parser='html.parser', **kwargs):
    nodes = []
    html = BeautifulSoup(raw_html, parser)
    for node in html.find_all(**kwargs):
        nodes.append(node)
    return nodes


def download_wallpaper(dl_url, path):
    if dl_url.find('/') and is_downloadable(dl_url):
        filename = dl_url.rsplit('/', 1)[1]
        absolute_path = os.path.join(path, filename)
        resp = requests.get(dl_url, allow_redirects=True)
        try:
            resp.raise_for_status()
            open(absolute_path, 'wb').write(resp.content)
        except Exception as exc:
            print('There was a problem: %s' % exc)
            print('Failed')
    else:
        print('Incorrect url: {0}'.format(dl_url))
        print('Failed')


def download_wallpapers(*agrs, **kwargs):
    print('Started working...')

    rwidth = kwargs['rwidth']
    rheight = kwargs['rheight']
    resolution = '{0}x{1}'.format(rwidth, rheight)
    pages = kwargs['pages']
    month, year = datetime.now().replace(year=kwargs['year'],
                                         month=kwargs['month']).strftime('%m,%Y').split(',')
    category = '/{0}/{1}/'.format(year, month)
    download_folder = os.path.join(BASE_DIR, resolution, year, month)
    base_url = BASE_URL

    make_dirs(download_folder)

    for page_number in range(1, pages + 1):
        print('Searching wallpapers in {0} category on {1}...'.format(category, base_url))
        page_list = simple_get(base_url)
        if page_list:
            def startswith_year_and_month_and_endswith_slash(href):
                return href and href.startswith(category) and href.endswith('/')

            wl_nodes = find_nodes(raw_html=page_list, parser='html.parser',
                                  name='a', href=startswith_year_and_month_and_endswith_slash, class_='')
            print('Found {0} categories'.format(len(wl_nodes)))
            for wl_node in wl_nodes:
                wallpaper_dl_page_url = '{0}{1}'.format(BASE_URI, wl_node.attrs['href'])
                wallpaper_dl_page = simple_get(wallpaper_dl_page_url)
                if wallpaper_dl_page:
                    print('Searching wallpapers in {0} category'.format(wallpaper_dl_page_url))
                    wldl_nodes = find_nodes(raw_html=wallpaper_dl_page, parser='html.parser',
                                            name='a', string=resolution)
                    print('Found {0} wallpapers with specified resolution {1} on {2}'.format(len(wldl_nodes),
                                                                                             resolution,
                                                                                             wallpaper_dl_page_url))
                    for wldl_node in wldl_nodes:
                        wallpaper_dl_url = wldl_node.attrs['href']
                        print('Downloading {0}...'.format(wallpaper_dl_url))
                        download_wallpaper(dl_url=wallpaper_dl_url, path=download_folder)
                        print('Done')
        base_url = '{0}/page/{1}/'.format(BASE_URL, page_number)

    print('Finished')
