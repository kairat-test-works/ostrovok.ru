# Usage

`$ python swd-cli.py [-h] [-rw RWIDTH] [-rh RHEIGHT] [-p PAGES] [-y YEAR] [-m {1,2,3,4,5,6,7,8,9,10,11,12}]
`

Run `python swd-cli.py -h` to see help

### List of options

All options are optional and have default values.

* `-y YYYY` or `--year=YYYY`: to specify a year. Default is current year
* `-m M` or `--month=M`: to specify a month. Default is current month
* `-rw RWIDTH` or `--rwidth=RWIDTH`: to specify width resolution. Default is 1366
* `-rw RHEIGHT` or `--rheight=RHEIGHT`: to specify height resolution. Default is 768
* `-p PAGES` or `--pages=PAGES`: to specify maximum number of pages to search. Default is 1
