import argparse
from datetime import datetime

import swd

if __name__ == "__main__":
    current_datetime = datetime.now()

    parser = argparse.ArgumentParser(
        description='Download wallpapers from {0}'.format(swd.BASE_URI)
    )
    parser.add_argument('-rw', '--rwidth', type=str, default='1366',
                        help='Specify wallpaper width resolution to download. Format -rw=WIDTH, e.g "-rw 1366". ' +
                             'By default is -rw=1366')
    parser.add_argument('-rh', '--rheight', type=str, default='768',
                        help='Specify wallpaper height resolution to download. Format -rh=HEIGHT, e.g "-rh 768". ' +
                             'By default is -rh=768')
    parser.add_argument('-p', '--pages', type=int, default=1,
                        help='Specify number of pages to search. Format -p=5, e.g -p 2018. ' +
                             'By default is 1')
    parser.add_argument('-y', '--year', type=int, default=current_datetime.year,
                        help='Specify by what year to search wallpapers. Format -y=YYYY, e.g -y 2018. ' +
                             'By default is current year')
    parser.add_argument('-m', '--month', type=int, default=current_datetime.month,
                        choices=[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
                        help='Specify by which month to search wallpapers. Format -m=MM, e.g for July: -m 7. ' +
                             'By default is current month')

    args = parser.parse_args()
    swd.download_wallpapers(rwidth=args.rwidth,
                            rheight=args.rheight,
                            pages=args.pages,
                            year=args.year,
                            month=args.month)
