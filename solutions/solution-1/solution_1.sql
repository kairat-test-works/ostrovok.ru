SELECT
  emp.department,
  emp.name,
  emp.salary
FROM employee emp
WHERE emp.salary IN (
  SELECT max(em.salary)
  FROM employee em
  GROUP BY em.department
)
ORDER BY emp.department ASC;

SELECT
  DISTINCT emp.department
FROM employee emp
ORDER BY emp.department ASC;


